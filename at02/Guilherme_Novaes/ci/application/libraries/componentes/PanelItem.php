<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Classe do componente Panel
 * 
 * @Author Guilherme Novaes
 * 
 */

class PanelItem{
    
    /**
     * Atributo Privado que recebe a classe do panel
     */
    private $classe;

     /**
     * Inicializa todos atributos com seus valores padrões.
     */
    function __construct($classe){
        $this->classe = $classe;
    }

    /**
     * Método público que gera o componente Panel e o retorna como HTML
     */
    public function getComponente1(){
        $html = '<div class="panel '.$this->classe.'">';
        $html .= $this->PanelHeader();
        $html .= '</div>';
 
        return $html;
    }

    /**
     * Método privado que gera o Panel Header e o retorna como HTML
     */
    private function PanelHeader(){
        $html = '<div class="panel-heading">Panel Header</div>';
        return $html;
    }

    /**
    * Método privado que gera o Panel Body e o retorna como HTML
    */
    private function PanelBody(){
        $html = '<div class="panel-body">Panel Content</div>';
        return $html;
    }

    /**
    * Método privado que gera o Panel Footer e o retorna como HTML
    */
    private function PanelFooter(){
        $html = '<div class="panel-footer">Panel Footer</div>';
        return $html;
    }


    /**
    * Método público que gera o Panel somente com o body e Header e o retorna como HTML
    */
    public function getExemplo2(){
        $html = '<div class="panel '.$this->classe.'">';
        $html .= $this->PanelHeader();
        $html .= $this->PanelBody();
        $html .= '</div>';
 
        return $html;
    }

    /**
    * Método público que gera o Panel somente com o body e Footer e o retorna como HTML
    */
    public function getExemplo3(){
        $html = '<div class="panel '.$this->classe.'">';
        $html .= $this->PanelBody();
        $html .= $this->PanelFooter();
        $html .= '</div>';
 
        return $html;
    }
} 