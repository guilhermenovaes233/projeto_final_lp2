<?php 

class ProgressBarModel extends CI_Model {
    private $bar_data = array(
        array('progress-bar'),
        array('25')
    );
    
    public function ProgressBar() {
        $html = '<div class="single_courses">';
        $html .= $this->ProgressBarBody($this->bar_data); 
        $html .= '</div>';
        return $html;
    }

    private function ProgressBarBody($data){
        $cont = 1;
        $html = '<div class="progress">';  
            $html = '<div class="progress-bar"  role="progressbar" ';
            $html .= 'style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>';
        $html .= '</div>';
        return $html;
    }
    

    public function detalhe($id){
        return $this->bar_data[$id];
    }

}
?> 
