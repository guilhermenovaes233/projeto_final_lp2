<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Inclui a classe do Panel
 */
include APPPATH.'libraries/componentes/PanelItem.php';

/**
 * Classe da model Panel
 * 
 * @Author Guilherme Novaes
 * 
 */
class PanelModel extends CI_Model{

    /**
     * Função pública para gerar e recuperar uma panel para o exemplo inicial
     */
    public function getComponente(){
        $lista = '';
        $data = new PanelItem('panel-default');
        $lista .= $data->getComponente1();

        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma panel para o exemplo 2
     */
    public function getExemplo2(){
        $lista = '';
        $data = new PanelItem('panel-default');
        $lista .= $data->getExemplo2();

        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma panel para o exemplo 3
     */
    public function getExemplo3(){
        $lista = '';
        $data = new PanelItem('panel-default');
        $lista .= $data->getExemplo3();

        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma panel para o exemplo 4
     */
    public function getComponentes(){
        $lista = '';

        $progress_data = array(
            $x0 = 'panel-default',
            $x1 = 'panel-primary',
            $x2 = 'panel-success',
            $x3 = 'panel-info',
            $x4 = 'panel-warning',
            $x5 = 'panel-danger',
        );

        //Percorre a lista dos componentes
        foreach($progress_data as $row){
            $data = new PanelItem($row);
            $lista .= $data->getComponente1();
        }
            
        return $lista;
    }
}