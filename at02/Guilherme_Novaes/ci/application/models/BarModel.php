<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Inclui a classe do ProgressBar
 */
include APPPATH.'libraries/componentes/BarItem.php';

/**
 * Classe da model ProgressBar
 * 
 * @Author Guilherme Novaes
 * 
 */
class BarModel extends CI_Model{

    /**
     * Função pública para gerar e recuperar uma progressBar para o exemplo inicial
     */
    public function getComponente(){
        $lista = '';

        for($cont = 0; $cont <=  100; $cont +=25){
            $data = new BarItem('progress-bar', $cont,'');
            $lista .= $data->getComponente();
        }
        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma progressBar para o exemplo 1
     */
    public function getComponente1(){
        $lista = '';

        $progress_data = array(
            $x0 = 'progress-bar bg-success ',
            $x1 = 'progress-bar bg-info ',
            $x2 = 'progress-bar bg-warning ',
            $x3 = 'progress-bar bg-danger'        
        );

        $i = 25;
        //Percorre a lista dos componentes
        foreach($progress_data as $row){
            $data = new BarItem($row, $i,'');
            $lista .= $data->getComponente();
            $i += 25;
        }
            
        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma progressBar para o exemplo 2
     */
    public function getComponente2(){
        $lista = '';

        $progress_data = array(
            $x0= 'progress-bar progress-bar-striped',
            $x1= 'progress-bar progress-bar-striped bg-success',
            $x2= 'progress-bar progress-bar-striped bg-info',
            $x3= 'progress-bar progress-bar-striped bg-warning',
            $x4= 'progress-bar progress-bar-striped bg-danger '     
        );

        $i = 10;
        //Percorre a lista dos componentes
        foreach($progress_data as $row){
            $data = new BarItem($row, $i, '');
            $lista .= $data->getComponente();
            $i += 18;
        }
            
        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma progressBar para o exemplo 3
     */
    public function getComponente3(){
        $lista = '';

        $progress_data = array(
            $x0= 'progress-bar progress-bar-striped progress-bar-animated'   
        );

        //Percorre a lista dos componentes
        foreach($progress_data as $row){
            $data = new BarItem($row, 75, '');
            $lista .= $data->getComponente();
        }
            
        return $lista;
    }

    /**
     * Função pública para gerar e recuperar uma progressBar para o exemplo 4
     */
    public function getComponente4(){
        $lista = '';

        $progress_data = array(
            $x0= 'style="height: 1px;"',
            $x1= 'style="height: 20px;"',
        );

        //Percorre a lista dos componentes
        foreach($progress_data as $row){
            $data = new BarItem('progress-bar', 75, $row);
            $lista .= $data->getComponente();
        }
            
        return $lista;
    }





    /*
        //Barras multiplas
        progress-bar
        progress-bar bg-success
        progress-bar bg-info
    */
}