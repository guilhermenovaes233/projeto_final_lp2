
<body>
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid p-0">
                    <div class="row align-items-center no-gutters">
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo-img">
                                <a href="<?php echo base_url();?>">
                                    <img src="assets/img/logo.PNG" alt="">
                                </a>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-7">
                            <div class="main-menu  d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="active" href="<?php echo base_url();?>"> home</a></li>
                                        <li><a href="<?php echo base_url('about');?>"> Sobre </a></li>
                                        <li><a href="<?php echo base_url('cursos');?>"> Cursos </a></li>
                                        <li><a href="<?php echo base_url();?>"> Componentes <i class="ti-angle-down"></i></a>
                                            <ul class="submenu">
                                                <li><a href="<?php echo base_url('viewProgressBar');?>"> Progress Bar </a></li>
                                                <li><a href="<?php echo base_url('viewPanel');?>"> Panel </a></li>
                                                <li><a href="<?php echo base_url('viewPagination');?>"> Pagination </a></li>
                                            </ul>
                                        </li>
                                        
                                        
                                        <li><a href="<?php echo base_url('contato');?>"> contato </a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                            <div class="log_chat_area d-flex align-items-center">
                                <div class="live_chat_btn">
                                    <a class="boxed_btn_orange" href="#">
                                        <i class="fa fa-phone"></i>
                                        <span>11 97070-7070</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->
