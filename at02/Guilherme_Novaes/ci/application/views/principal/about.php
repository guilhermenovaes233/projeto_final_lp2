    <!-- bradcam_area_start -->
        <div class="bradcam_area breadcam_bg overlay2">
            <h3>Sobre </h3>
        </div>
        <!-- bradcam_area_end -->

    <!-- about_area_start -->
    <div class="about_area">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6">
                    <div class="single_about_info">
                        <h3>O que são <br>
                            Componentes?</h3>
                         <p> Para obtenção de produtos de software com alta qualidade e que sejam economicamnete viáveis, faz-se extremamente
                         necessário a adoção de um conjunto sistemático de processos, técnicas e ferramentas.
                         <br/>Reutilizando partes bem especificadas, desenvolvidas e testadas, pode-se construir 
                         software em menor tempo e com maior confiabilidade
                         <br/>O desenvolvimento baseado em componentes(DBC) surgiu como uma nova perspectiva para
                         o desenvolvimento de software, cujo objetivo é a fragmentação de blocos monolíticos em componentes que interagem
                         entre si
                         <br/>Reduzindo, desta forma, a complexidade do desenvolvimento de sistemas, assim como os seus custos, 
                         através da utilização de componentes que, em princípio,
                         seriam adequados para serem utilizados em outras aplicações.</s></p>

                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-6">
                    <div class="about_tutorials">
                        <div class="courses">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>20+</span>
                                    <p> Courses</p>
                                </div>
                            </div>
                        </div>
                        <div class="courses-blue">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>7638</span>
                                    <p> Courses</p>
                                </div>

                            </div>
                        </div>
                        <div class="courses-sky">
                            <div class="inner_courses">
                                <div class="text_info">
                                    <span>230+</span>
                                    <p> Courses</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- about_area_end -->

    <!-- our_team_member_start -->
    <div class="our_team_member">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 col-md-6 col-lg-3">
                    <div class="single_team">
                        <div class="thumb">
                            <img src="assets/img/team/Guilherme1.jpg" alt="">
                            <div class="social_link">
                                <a href=""><i class="fa fa-envelope"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="master_name text-center">
                            <h3> Guilherme Novaes </h3>
                            <p> Desenvolvedor FullStack</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- our_team_member_end -->