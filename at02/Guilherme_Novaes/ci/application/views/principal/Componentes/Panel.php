
<!-- bradcam_area_start -->
     <div class="courses_details_banner">
         <div class="container">
             <div class="row">
                 <div class="col-xl-6">
                     <div class="course_text">
                            <h3>Panel</h3>
                            <div class="rating">
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <span>(5)</span>
                            </div>
                            <div class="hours">
                                <div class="video">
                                     <div class="single_video">
                                            <i class="fa fa-clock-o"></i> 
                                            <span> Videos</span>
                                     </div>
                                </div>
                            </div>
                     </div>
                 </div>
             </div>
         </div>
    </div>
    <!-- bradcam_area_end -->

    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="single_courses">
                    <h3 class="second_title">Dicas</h3>
                    </div>
                    <div class="outline_courses_info">
                            <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <i class="flaticon-question"></i> De onde esse conteúdo foi tirado?
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                               Esse conteúdo pode ser encontrado no site 
                                               <a href="https://www.w3schools.com/bootstrap/bootstrap_panels.asp"> <u> w3schools </u>  </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <i class="flaticon-question"></i>Basic Classes</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                                let god moving. Moving in fourth air night bring upon
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="courses_sidebar">
                        <div class="video_thumb">
                            <img src="assets/img/courses/panel.png" alt="">
                            <a class="popup-video" href="">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="assets/img/team/Guilherme1.jpg" style="width: 50%;" alt="">
                                </div>
                                <div class="name">
                                    <h3>Guilherme Novaes</h3>
                                    <p>Desenvolvedor Full Stack</p>
                                </div>
                            </div>
                            <p class="text_info">
                                Estudante do curso de análise e desenvolvimento de sistemas
                            </p>
                            <ul>
                                <li><a href="#"> <i class="fa fa-envelope"></i> </a></li>
                                <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                <li><a href="#"> <i class="ti-linkedin"></i> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

    <br/><br/>

    <div class="single_courses">
        <h3>  Painéis </h3>
        <p> Um painel no bootstrap é uma caixa com bordas com algum preenchimento em torno do seu conteúdo:</p>
    </div>
    <br/>
    <div class="single_courses">
        <?= $exemplo1 ?>
        <br/>
        <p> Os painéis são criados com a
            .panelclasse e o conteúdo dentro do painel tem uma .panel-bodyclasse:</p>
        <br/>

        <textarea readonly style="width: 100%; height: 100px;"> 
            <?= $exemplo1 ?>
        </textarea> 
    </div>
    <br/>  

    <div class="single_courses">
        <h3> Painéis com Classes Contextuais </h3>
        <p>Para colorir o painel, usar classes contextuais ( .panel-default, .panel-primary, 
            .panel-success, .panel-info, .panel-warning, ou .panel-danger):</p>
    </div>
    <br/>
    <div class="single_courses">
        
    <div class="panel-group">
        <?= $exemplo4 ?>
    </div>
        <br/>
        <textarea readonly style="width: 100%; height: 150px;"> 
            <?= $exemplo4 ?>
        </textarea> 
    </div>
    <br/> 
     
    <div class="single_courses">
        <h3>  Cabeçalho do painel </h3>
    </div>
    <br/>
    <div class="single_courses">
        <?= $exemplo2 ?>
        <br/>
        <p> A .panel-heading classe adiciona um cabeçalho ao painel:</p>
        <br/>

        <textarea readonly style="width: 100%; height: 100px;"> 
            <?= $exemplo2 ?>
        </textarea> 
    </div>
    <br/>  

    <div class="single_courses">
        <h3>  Rodapé do painel </h3>
    </div>
    <br/>
    <div class="single_courses">
        <?= $exemplo3?>
        <br/>
        <p> A .panel-footerclasse adiciona um rodapé ao painel: </p>
        <br/>

        <textarea readonly style="width: 100%; height: 100px;"> 
        <?= $exemplo3?>
        </textarea> 
    </div>
    <br/>  

    <div class="single_courses">
        <h3>  Grupo do Painel </h3>
    </div>
    <br/>
    <div class="single_courses">
        <div class="panel-group">
            <?= $exemplo2 ?>
        </div>
        <br/>
        <p> Os painéis são criados com a
            .panelclasse e o conteúdo dentro do painel tem uma classe .panel-body :</p>
        <br/>

        <textarea readonly style="width: 100%; height: 150px;"> 
            <div class="panel-group">
                <?= $exemplo2 ?>
            </div>
        </textarea> 
    </div>
    <br/>  

   
</div>
</div>
</div>