
     <!-- bradcam_area_start -->
     <div class="courses_details_banner">
         <div class="container">
             <div class="row">
                 <div class="col-xl-6">
                     <div class="course_text">
                            <h3> Pagination </h3>
                            <div class="rating">
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <i class="flaticon-mark-as-favorite-star"></i>
                                <span>(5)</span>
                            </div>
                            <div class="hours">
                                <div class="video">
                                     <div class="single_video">
                                            <i class="fa fa-clock-o"></i> 
                                            <span> Videos</span>
                                     </div>
                                </div>
                            </div>
                     </div>
                 </div>
             </div>
         </div>
    </div>
    <!-- bradcam_area_end -->

    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="single_courses">
                    <h3 class="second_title">Dicas</h3>
                    </div>
                    <div class="outline_courses_info">
                            <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                    <i class="flaticon-question"></i> De onde esse conteúdo foi tirado?
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                            <div class="card-body">
                                               Esse conteúdo pode ser encontrado no site 
                                               <a href="https://getbootstrap.com/docs/4.0/components/pagination/"> <u> Getbootstrap </u> </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                    <i class="flaticon-question"></i>Basic Classes</span>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                            <div class="card-body">
                                                Our set he for firmament morning sixth subdue darkness creeping gathered divide our
                                                let god moving. Moving in fourth air night bring upon
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="courses_sidebar">
                        <div class="video_thumb">
                            <img src="assets/img/courses/progress-bar-square.jpg" alt="">
                            <a class="popup-video" href="https://www.youtube.com/watch?v=093pKz4YzY4">
                                <i class="fa fa-play"></i>
                            </a>
                        </div>
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="assets/img/team/Guilherme1.jpg" style="width: 50%;" alt="">
                                </div>
                                <div class="name">
                                    <h3>Guilherme Novaes</h3>
                                    <p>Desenvolvedor Full Stack</p>
                                </div>
                            </div>
                            <p class="text_info">
                                Estudante do curso de análise e desenvolvimento de sistemas
                            </p>
                            <ul>
                                <li><a href="#"> <i class="fa fa-envelope"></i> </a></li>
                                <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                <li><a href="#"> <i class="ti-linkedin"></i> </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

    <br/><br/>
    <div class="single_courses">
        <h3> Como funciona </h3>
        <p>     Utilizamos um grande bloco de links conectados para a nossa paginação, tornando os links
            difíceis de perder e facilmente escaláveis ​​- ao mesmo tempo em que proporcionamos grandes
            áreas de acesso. A paginação é criada com elementos HTML da lista para que os leitores de
            tela possam anunciar o número de links disponíveis. 
            Use um "nav" elemento de quebra automática para identificá-lo como uma seção de navegação 
            para rastrear leitores e outras tecnologias assistivas.
                Além disso, como as páginas provavelmente possuem mais de uma seção de navegação, 
            é aconselhável fornecer um descritivo aria-label para "nav"
            refletir seu objetivo. Por exemplo, se o componente de 
            paginação for usado para navegar entre um conjunto de resultados da pesquisa, 
            um rótulo apropriado poderá ser aria-label="Search results pages".</p>

            <br/>
            <?= $exemplo1 ?>
            <br/>
            <textarea readonly style="width: 100%; height: 250px;"> 
            <?= $exemplo1 ?>
            </textarea>
    </div>
    <br/>

    <div class="single_courses">
        <h3> Trabalhando com ícones </h3>
        <p> Deseja usar um ícone ou símbolo no lugar do texto em alguns links de paginação? Certifique-se de fornecer suporte adequado ao leitor de tela com aria
            atributos e o .sr-onlyutilitário.</p>
        <br/>
        <nav aria-label="Page navigation example">
            <?= $exemplo4 ?>
        </nav>
        <br/>
        <textarea readonly style="width: 100%; height: 400px;"> 
            <?= $exemplo4 ?>
        </textarea> 
        <br/>
    </div>
    <br/>


    <div class="single_courses">
        <h3> Estados desativados e ativos </h3>
        <p> Os links de paginação são personalizáveis ​​para diferentes circunstâncias. 
        Use .disabledpara links que parecem não clicáveis ​​e .activepara indicar a página atual.
        Enquanto a .disabledclasse usa pointer-events: nonepara tentar desativar a 
        funcionalidade de link de <a>s, essa propriedade CSS ainda não está padronizada e 
        não é responsável pela navegação no teclado. Como tal, você sempre deve adicionar 
        tabindex="-1"links desabilitados e usar JavaScript personalizado para desativar 
        totalmente sua funcionalidade.</p>
        <br/>
        <nav aria-label="...">
            <?= $exemplo5 ?>
        </nav>
        <br/>
        <textarea readonly style="width: 100%; height: 250px;"> 
        <nav aria-label="...">
            <?= $exemplo5 ?>
        </nav>
        </textarea> 
        <br/>
    </div>
    <br/>


    <div class="single_courses">
        <h3> Dimensionamento </h3>
        <p>Gosta de paginação maior ou menor? Adicione .pagination-lgou .pagination-smpara tamanhos adicionais.</p>
        <br/>
        <nav aria-label="...">
            <?= $exemplo3 ?>
        </nav>
        <br/>
        <textarea readonly style="width: 100%; height: 250px;"> 
            <nav aria-label="...">
                <?= $exemplo3 ?>
            </nav>
        </textarea> 
        <br/>
    </div>
    <br/>

    <div class="single_courses">
        <h3> Alinhamento </h3>
        <p> Altere o alinhamento dos componentes de paginação com os utilitários flexbox. </p>
        <br/>
        <?= $exemplo2 ?>
        <br/>
        <textarea readonly style="width: 100%; height: 250px;"> 
            <?= $exemplo2 ?>
        </textarea> 
        <br/>
    </div>
    <br/>
    
</div>
</div>
</div>