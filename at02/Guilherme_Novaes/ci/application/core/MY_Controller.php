<?php
defined("BASEPATH") OR exit("No direct script access allowed");

class MY_Controller extends CI_Controller{
    public function show($conteudo){

      //incluir cabeçalho
      $html =  $this->load->view('common/header');
      $html .= $this->load->view('common/topo');

      //incluir o conteudo
      $html .= $conteudo;

      //incluir o rodape 
      $html .= $this->load->view('common/footer');

      echo $html;

    }
}


?>